// importacion de endpoints creados
const getPosts = require("./getPosts");
const createPost = require("./createPost");
const deletePost = require("./deletePost");
// carga las variables de entorno al proyecto
require('dotenv');

// montar todos los endpoints importados
module.exports = function(app) {
    console.info("Inicio montado de endpoints");
    const PREFIX = process.env.API_PREFIX
    // console.debug(`prefijo de api: "${PREFIX}"`)
    
    app.use(PREFIX, getPosts); // dominio/api/posts GET
    app.use(PREFIX, createPost); // dominio/api/post POST
    app.use(PREFIX, deletePost); // dominio/api/post/:postId DELETE

    console.info("Endpoints montados correctamente")
};