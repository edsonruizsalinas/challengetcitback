const express = require("express");
const router = express.Router();

router.delete("/post/:postId", (req, response, next) => {
    // contectamos con la bd
    const connection = require("../postgreConnector");
    // query a realizar
    var queryInsert = `DELETE \
        FROM tbl_posts \
        WHERE id = $1`;

    // console.debug(`consulta insert ${queryInsert}`);

    var postId = req.params.postId;

    // console.debug(`postId: ${postId}`);

    if (!postId) {
        // si no existe id error
        console.error("No se ha proporcionado id del post");
        return response.status(400);
    }

    connection.query(
        queryInsert,
        [postId],
        (error, results) => {
        if (error) {
          throw error;
        }
        return response.status(200).json({
            message: "post eliminado correctamente"
        });
    })
});

module.exports = router;