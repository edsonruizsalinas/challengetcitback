const express = require("express");
const router = express.Router();

// Obtenemos todos los posts
router.get("/posts", (req, response, next) => {
  // conectamos con la BD
    const connection = require("../postgreConnector");
    var querySelect = "SELECT * FROM tbl_posts ORDER BY id ASC";
    // console.debug(`query select: ${querySelect}`)

    connection.query(
        querySelect,
        (error, results) => {
          if (error) {
            throw error;
          }

          if (results.rowCount === 0) {
            console.info("No hay datos para retornar");
            return response.status(200).json({
              message: "No hay datos para mostrar"
            });
          }

          console.info("Fin funcion getPosts");
          return response.status(200).json({
            message: "datos obtenidos correctamente",
            resultados: results.rows
          });
    });
});

module.exports = router;