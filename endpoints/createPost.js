const express = require("express");
const router = express.Router();

router.post("/post", (req, response, next) => {
    const connection = require("../postgreConnector");

    var queryInsert = `INSERT INTO \
        tbl_posts (nombre, descripcion) \
        VALUES ($1, $2)`;

    console.debug(`consulta insert ${queryInsert}`);

    var nombre = req.body.nombre;
    var descripcion = req.body.descripcion;

    console.debug(`nombre: ${nombre}`);
    console.debug(`descripcion: ${descripcion}`);

    if (!nombre && !descripcion) {
        console.error("No se ha proporcionado nombre o descripcion");
        return response.status(400);
    }
    console.debug(" iniciando insercion de datos")
    connection.query(
        queryInsert,
        [nombre, descripcion],
        (error, results) => {
        console.debug("Entro a la insercion");
        if (error) {
          throw error;
        }
        return response.status(200).json({
            message: "datos insertados correctamente"
        });
    })
});

module.exports = router;