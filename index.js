const express = require("express");
const app = express();
const http = require('http');
const cors = require('cors');
const bodyParser = require('body-parser');
require('dotenv').config({
  path: './.env'
})

const PORT = process.env.PORT || 3001;
const server = http.createServer(app);

// configuración middleware
app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// rutas
require('./endpoints/routes')(app);

// montar servidor
server.listen(PORT, () => {
  console.log(`Servidor escuchando en el puerto: ${PORT}`);
});

