// Nos conectamos a la bd

const { Client } = require("pg");
require("dotenv").config({ path: "./.env" });

const USER = process.env.DB_USER;
const HOST = process.env.DB_HOST;
const DATABASE = process.env.DB_DATABASE;
const PASSWORD = process.env.DB_PASSWORD;
const PORT = process.env.DB_PORT;

// console.info("Iniciando conexion");
// console.debug(`User: ${USER}`);
// console.debug(`HOST: ${HOST}`);
// console.debug(`DATABASE: ${DATABASE}`);
// console.debug(`PASSWORD: ${PASSWORD}`);
// console.debug(`PORT: ${PORT}`);

const connection = new Client({
  user: USER,
  database: DATABASE,
  password: PASSWORD,
  port: PORT,
  host: HOST,
});

connection.connect(function (err) {
  if (err) throw err;
  console.log(`[PostgreConnector] Conectado a ${HOST} correctamente.`);
});

module.exports = connection;
// exportamos para su uso
